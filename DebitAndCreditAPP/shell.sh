#使用方法
echo $0

if [ ! -d ./IPADir ];
then
mkdir -p IPADir;
fi

#工程绝对路径  /Users/juyoo/Desktop/爱家物联
project_path="/Users/hxpp.com/chain_sharing/DebitAndCreditAPP/"

#工程名 将XXX替换成自己的工程名
project_name=DebitAndCreditAPP

#scheme名 将XXX替换成自己的sheme名
scheme_name=DebitAndCreditAPP

#打包模式 Debug/Release
development_mode=Debug

#build文件夹路径
build_path=${project_path}/build

#plist文件所在路径
exportOptionsPlistPath=${project_path}/exportTest.plist

#导出.ipa文件所在路径
exportIpaPath=${project_path}/IPADir/${development_mode}




echo "Place enter the number you want to export ? [ 1:app-store 2:ad-hoc] "

##
read number
while([[ $number != 1 ]] && [[ $number != 2 ]])
do
echo "Error! Should enter 1 or 2"
echo "Place enter the number you want to export ? [ 1:app-store 2:ad-hoc] "
read number
done

if [ $number == 1 ];then
development_mode=Release
exportOptionsPlistPath=${project_path}/exportAppstore.plist
else
development_mode=Debug
exportOptionsPlistPath=${project_path}/exportTest.plist
fi


echo '///-----------'
echo '/// 正在清理工程'
echo '///-----------'
xcodebuild \
clean -configuration ${development_mode} -quiet  || exit


echo '///--------'
echo '/// 清理完成'
echo '///--------'
echo ''

echo '///-----------'
echo '/// 正在编译工程:'${development_mode}
echo '///-----------'
xcodebuild \
archive -workspace ${project_path}/${project_name}.xcworkspace \
-scheme ${scheme_name} \
-configuration ${development_mode} \
-archivePath ${build_path}/${project_name}.xcarchive  -quiet  || exit

echo '///--------'
echo '/// 编译完成'
echo '///--------'
echo ''

echo '///----------'
echo '/// 开始ipa打包'
echo '///----------'
xcodebuild -exportArchive -archivePath ${build_path}/${project_name}.xcarchive \
-configuration ${development_mode} \
-exportPath ${exportIpaPath} \
-exportOptionsPlist ${exportOptionsPlistPath} \
-quiet || exit

if [ -e $exportIpaPath/$scheme_name.ipa ]; then
echo '///----------'
echo '/// ipa包已导出'
echo '///----------'
open $exportIpaPath
else
echo '///-------------'
echo '/// ipa包导出失败 '
echo '///-------------'
fi
echo '///------------'
echo '/// 打包ipa完成  '
echo '///-----------='
echo ''

echo '///-------------'
echo '/// 开始发布ipa包 '
echo '///-------------'

if [ $number == 1 ];then

#验证并上传到App Store
# 将-u 后面的XXX替换成自己的AppleID的账号，-p后面的XXX替换成自己的密码
altoolPath="/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Versions/A/Support/altool"
"$altoolPath" --validate-app -f ${exportIpaPath}/${scheme_name}.ipa -u 13358372170@163.com -p binjiejidianBJJD123 -t ios --output-format xml
"$altoolPath" --upload-app -f ${exportIpaPath}/${scheme_name}.ipa -u  13358372170@163.com -p binjiejidianBJJD123 -t ios --output-format xml
else

#上传到Fir
# 将XXX替换成自己的Fir平台的token
#fir login -T dcdaf9a7ace0e4f19597a488ea4c8fa0
#fir publish $exportIpaPath/$scheme_name.ipa
# 配置蒲公英
pgy_userKey="83e9ce732560254d21ad8f971879b1fa"
pgy_apiKey="ad3a0ea14525ff035fa15268642c73e9"

curl -F "file=@$exportIpaPath/${project_name}.ipa" -F "uKey=$pgy_userKey" -F "_api_key=$pgy_apiKey" https://www.pgyer.com/apiv1/app/upload
fi

exit 0


