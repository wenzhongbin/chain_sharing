//
//  UIView+push.swift
//  Skycar
//
//  Created by hxpp on 2018/5/30.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

private let HXWEBVIEW = "HXWEBVIEW"
private let ALERTVIEW_CITY = "ALERTVIEW_CITY"
private let RIDINGVIEW = "RIDINGVIEW"
private let LUGGAGEVIEW = "LUGGAGEVIEW"
private let DATEPICKERVIEW = "DATEPICKERVIEW"
private let PAYVIEW = "PAYVIEW"
private let CALENDARVIEW = "CALENDARVIEW"
private let TITLEHINTVIEW = "TITLEHINTVIEW"
private let DWVERSIONVIEW = "DWVERSIONVIEW"
private let DWNODATAVIEW = "DWNODATAVIEW"
private let DWALERTTABLEVIEW = "DWALERTTABLEVIEW"
private let SIGNALERTVIEW = "SIGNALERTVIEW"
private let DATEPICKERVIEW_TIME = "DATEPICKERVIEW_TIME"
private let JSDATAVIEW = "JSDATAVIEW"
private let DWALERTVIEW = "DWALERTVIEW"
private let DWSHEETVIEW = "DWSHEETVIEW"



extension UIView {
    
//    class var dwSheetView : DWSheetView {
//        get{
//            if objc_getAssociatedObject(self, DWSHEETVIEW) != nil {
//                return objc_getAssociatedObject(self, DWSHEETVIEW) as! DWSheetView
//            }
//            let view = Bundle.main.loadNibNamed("DWSheetView", owner: nil, options: nil)?.first as! DWSheetView
//            objc_setAssociatedObject(self, DWSHEETVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var dw_AlertView : DW_AlertView {
//        get{
//            if objc_getAssociatedObject(self, DWALERTVIEW) != nil {
//                return objc_getAssociatedObject(self, DWALERTVIEW) as! DW_AlertView
//            }
//            let view = Bundle.main.loadNibNamed("DW_AlertView", owner: nil, options: nil)?.first as! DW_AlertView
//            objc_setAssociatedObject(self, DWALERTVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var jsDataView : JSDataView {
//        get{
//            if objc_getAssociatedObject(self, JSDATAVIEW) != nil {
//                return objc_getAssociatedObject(self, JSDATAVIEW) as! JSDataView
//            }
//            let view = Bundle.main.loadNibNamed("JSDataView", owner: nil, options: nil)?.first as! JSDataView
//            objc_setAssociatedObject(self, JSDATAVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//
//        }
//    }
//
//    class var datePickerView : DatePickerView {
//        get{
//            if objc_getAssociatedObject(self, DATEPICKERVIEW_TIME) != nil {
//                return objc_getAssociatedObject(self, DATEPICKERVIEW_TIME) as! DatePickerView
//            }
//            let view = Bundle.main.loadNibNamed("DatePickerView", owner: nil, options: nil)?.first as! DatePickerView
//            objc_setAssociatedObject(self, DATEPICKERVIEW_TIME, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var signAlertView : SignAlertView {
//        get{
//
//            if objc_getAssociatedObject(self, SIGNALERTVIEW) != nil {
//                return objc_getAssociatedObject(self, SIGNALERTVIEW) as! SignAlertView
//            }
//
//            let view = Bundle.main.loadNibNamed("SignAlertView", owner: nil, options: nil)?.first as! SignAlertView
//
//            objc_setAssociatedObject(self, SIGNALERTVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var alert_tableView : DW_AlertTableView {
//        get{
//
//            if objc_getAssociatedObject(self, DWALERTTABLEVIEW) != nil {
//                return objc_getAssociatedObject(self, DWALERTTABLEVIEW) as! DW_AlertTableView
//            }
//
//            let view = Bundle.main.loadNibNamed("DW_AlertTableView", owner: nil, options: nil)?.first as! DW_AlertTableView
//
//            objc_setAssociatedObject(self, DWALERTTABLEVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var nodata_view : DW_NoDataView {
//        get{
//
//            if objc_getAssociatedObject(self, DWNODATAVIEW) != nil {
//                return objc_getAssociatedObject(self, DWNODATAVIEW) as! DW_NoDataView
//            }
//
//            let view = Bundle.main.loadNibNamed("DW_NoDataView", owner: nil, options: nil)?.first as! DW_NoDataView
//
//            objc_setAssociatedObject(self, DWNODATAVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var version_view : DW_Version_View {
//        get{
//
//            if objc_getAssociatedObject(self, DWVERSIONVIEW) != nil {
//                return objc_getAssociatedObject(self, DWVERSIONVIEW) as! DW_Version_View
//            }
//
//            let view = Bundle.main.loadNibNamed("DW_Version_View", owner: nil, options: nil)?.first as! DW_Version_View
//
//            objc_setAssociatedObject(self, DWVERSIONVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var DW_WebView : DW_WebView {
//        get{
//
//            if objc_getAssociatedObject(self, HXWEBVIEW) != nil {
//                return objc_getAssociatedObject(self, HXWEBVIEW) as! DW_WebView
//            }
//
//            let view = Bundle.main.loadNibNamed("DW_WebView", owner: nil, options: nil)?.first as! DW_WebView
//
//            objc_setAssociatedObject(self, HXWEBVIEW, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
//
//    class var picker_view : DW_PickerView {
//        get{
//            if objc_getAssociatedObject(self, ALERTVIEW_CITY) != nil {
//                return objc_getAssociatedObject(self, ALERTVIEW_CITY) as! DW_PickerView
//            }
//            let view = Bundle.main.loadNibNamed("DW_PickerView", owner: nil, options: nil)?.first as! DW_PickerView
//            objc_setAssociatedObject(self, ALERTVIEW_CITY, view, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            return view
//        }
//    }
    
    func data(block:((_ showView:UIView)->Void)?) -> UIView {
        block?(self)
        return self
    }
    
    func showInWindow() {
        window()?.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalToSuperview()
        }
        self.show_animation()
        reloData()
    }
    
    func showInViewController() {

        window()?.rootViewController?.view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalToSuperview()
        }
        self.show_animation()
        reloData()
    }
    
    @objc func reloData() {}

    @objc func show_animation() {}
    
    @objc func diss_animation(completion: (()->Void)?) {
        completion?()
    }
    
    @objc func complete(data:((Any)->Void)?)->UIView {return self}

    func dissmiss() {
        
        diss_animation {
            self.removeFromSuperview()
        }
    }
    
    func window() -> UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    func parentViewController() -> UIViewController? {
        var n = self.next
        while n != nil {
            if (n is UIViewController) {
                return n as? UIViewController
            }
            n = n?.next
        }
        return nil
    }
    
}


extension UIView {
    class func push(vc:UIViewController?) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: noti_push), object: vc)
    }
    
    class func push_nav(vc:UIViewController?) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: noti_push_nav), object: vc)
    }
    
//    func visibleViewController() -> UIViewController? {
//        let vc : UITabBarController = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
//        
//        return (vc.selectedViewController as! BaseNavigetionViewController).visibleViewController
//    }
//    
//    class func visibleViewController() -> UIViewController? {
//        let vc : UITabBarController = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
//        
//        return (vc.selectedViewController as! BaseNavigetionViewController).visibleViewController
//    }
}



//extension UIViewController {
//    
//    
//    func is_show_placeholder_view(isShow:Bool) {
//        guard isShow else {
//            remove_placeholder_view()
//            return
//        }
//        add_placeholder_view(code: 0)
//    }
//    
//    func add_placeholder_view(code : Int) {
//        
//        remove_placeholder_view()
//        let placeholder = UIView.nodata_view.data { (v) in
//            code == 0 ? (v as! DW_NoDataView).setNoNetwork() : nil
//        }
//        guard placeholder != nil else {
//            return
//        }
//        self.view.addSubview(placeholder)
//        placeholder.snp.makeConstraints { (make) in
//            make.top.left.right.bottom.equalToSuperview()
//        }
//        self.view.bringSubview(toFront: placeholder)
//    }
//    
//    
//    func remove_placeholder_view() {
//        self.view.subviews.map { (v) -> Void in
//            if v.classForCoder == DW_NoDataView.classForCoder() {
//                v.removeFromSuperview()
//            }
//        }
//    }
//    
//    func bottom(b:NSLayoutConstraint) {
//        if UIDevice().isX() {
//            b.constant = -34
//        }
//    }
//    
//    func bottom_10(b:NSLayoutConstraint) {
//        if UIDevice().isX() {
//            b.constant = -34 + 10
//        }
//    }
//    
//    func top(b:NSLayoutConstraint) {
//        if UIDevice().isX() {
//            b.constant = -44
//        }else {
//            b.constant = -20
//        }
//    }
//}
