//
//  CGPath+link.swift
//  Skycar
//
//  Created by 温仲斌 on 2018/5/23.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import UIKit


func link_path(target view : UIView) -> CGPath {
    let br = UIBezierPath()
    br.move(to: CGPoint.init(x: 0, y: view.height))
    br.addLine(to: CGPoint.init(x: view.width, y: view.height))
    return br.cgPath
}



