//
//  UIView+value.swift
//  Skycar
//
//  Created by 温仲斌 on 2018/5/23.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation

import UIKit
/// MARK - UIVie

extension UIView {
    
    // MARK: - 常用位置属性
    public var left:CGFloat {
        get {
            return self.frame.origin.x
        }
        set(newLeft) {
            var frame = self.frame
            frame.origin.x = newLeft
            self.frame = frame
        }
    }
    
    public var top:CGFloat {
        get {
            return self.frame.origin.y
        }
        
        set(newTop) {
            var frame = self.frame
            frame.origin.y = newTop
            self.frame = frame
        }
    }
    
    public var width:CGFloat {
        get {
            return self.frame.size.width
        }
        
        set(newWidth) {
            var frame = self.frame
            frame.size.width = newWidth
            self.frame = frame
        }
    }
    
    public var height:CGFloat {
        get {
            return self.frame.size.height
        }
        
        set(newHeight) {
            var frame = self.frame
            frame.size.height = newHeight
            self.frame = frame
        }
    }
    
    public var right:CGFloat {
        get {
            return self.left + self.width
        }
    }
    
    public var bottom:CGFloat {
        get {
            return self.top + self.height
        }
    }
    
    public var centerX:CGFloat {
        get {
            return self.center.x
        }
        
        set(newCenterX) {
            var center = self.center
            center.x = newCenterX
            self.center = center
        }
    }
    
    public var centerY:CGFloat {
        get {
            return self.center.y
        }
        
        set(newCenterY) {
            var center = self.center
            center.y = newCenterY
            self.center = center
        }
    }
}

extension UIView {
    
    @IBInspectable var R_R : CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
            
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var B_C : UIColor {
        set {
            layer.borderColor = newValue.cgColor
            
        }
        get {
            return UIColor()
        }
    }
    
    @IBInspectable var B_W : CGFloat {
        set {
            layer.borderWidth = newValue
            
        }
        get {
            return layer.borderWidth
        }
    }
    
    open override func setValue(_ value: Any?, forKey key: String) {
        if key == "R_R" {
            self.layer.cornerRadius = value as! CGFloat
            self.layer.masksToBounds = true
            return
        }
        
        if key == "B_C" {
            self.layer.borderColor = (value as! UIColor).cgColor

            return
        }
        
        if key == "B_W" {
            self.layer.borderWidth = value as! CGFloat
  
            return
        }

        super.setValue(value, forKey: key)
    }
    
}

extension UIView {
    static func duplicate(_ view : UIView) -> UIView? {
        
        let data = NSKeyedArchiver.archivedData(withRootObject: view)
        
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? UIView
        
    }
    
    static func convertViewToImage(view : UIView) -> UIImage?{
        /*
         CGSize s = v.bounds.size;
         // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
         UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
         [v.layer renderInContext:UIGraphicsGetCurrentContext()];
         UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
         UIGraphicsEndImageContext();
 */
        let size = view.mj_size
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    static func x_l(_ view : UIView) {
        let dotteShapLayer = CAShapeLayer()
        dotteShapLayer.fillColor = UIColor.clear.cgColor
        dotteShapLayer.strokeColor = UIColor.orange.cgColor
        dotteShapLayer.lineWidth = 2.0
//        dotteShapLayer.path = mdotteShapePath
        let be = UIBezierPath.init(rect:  CGRect.init(origin: CGPoint.init(x: 10, y: 10), size: CGSize.init(width: view.width - 20, height: view.height - 20)))
        let arr :NSArray = NSArray(array: [10,5])
        dotteShapLayer.path = be.cgPath
        dotteShapLayer.lineDashPhase = 1.0
        dotteShapLayer.lineDashPattern = arr as? [NSNumber]
        view.layer.addSublayer(dotteShapLayer)
    }
}



extension NSLayoutConstraint {

    @IBInspectable var constant_or : CGFloat {
        set {
            let value = self.constant
            self.constant = value * m()
        }
        get {
            return 0
        }
    }
    
    func m() -> CGFloat{
        
        var sc : CGFloat = 1
        
        if sc_width() == 320 {
            sc = 0.5
        }
        
        return sc
    }
    
    open override func setValue(_ value: Any?, forKey key: String) {
        if key == "constant_sc" {
            return
        }
        super.setValue(value, forKey: key)
    }
}
