//
//  UIView+shadow.h
//  Skycar
//
//  Created by hxpp on 2018/6/8.
//  Copyright © 2018年 环球科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (shadow)
+ (UIView*)duplicate:(UIView*)view;

+ (void)addShadowToView:(UIView *)view
            withOpacity:(float)shadowOpacity
           shadowRadius:(CGFloat)shadowRadius
        andCornerRadius:(CGFloat)cornerRadius;
@end
