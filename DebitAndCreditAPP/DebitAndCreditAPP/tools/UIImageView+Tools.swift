//
//  UIImageView+Tools.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/7/31.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func image(_ name : String) {
        if name.contains("http") {
            self.kf.setImage(with: URL.init(string: name))
        }else {
            self.image = IMG(imStr: name)
        }
    }
}
