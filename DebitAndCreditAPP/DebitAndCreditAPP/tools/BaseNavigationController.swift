//
//  BaseNavigationController.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/7/18.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController,UINavigationControllerDelegate {
    
    let hx_navigation : UIView = UIView()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = UIColor.black

        delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(push_notification(noti:)), name: NSNotification.Name(rawValue: noti_push_nav), object: nil)
    }
    
    @objc func push_notification(noti:Notification) {
        let vc = (noti.object as! UIViewController)
    
        self.viewControllers.last?.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func needHiddenBar(in viewController : UIViewController) -> Bool {
        if viewController.classForCoder == LoginAccountViewController.classForCoder() {
            return true
        }
        
        return false
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        navigationController.setNavigationBarHidden(needHiddenBar(in: viewController), animated: animated)
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.done, target: nil, action: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
