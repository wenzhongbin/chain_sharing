//
//  UINavigationController+push.swift
//  Skycar
//
//  Created by hxpp on 2018/5/30.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import UIKit

let noti_push = "noti_tabBar_push"
let noti_push_nav = "noti_nav_push"

extension UITabBarController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(push_notification(noti:)), name: NSNotification.Name(rawValue: noti_push), object: nil)
    }
    
    @objc func push_notification(noti:Notification) {
        if ((noti.object as? UIViewController) != nil) {
            let vc = (noti.object as! UIViewController)
            let cu_vc = viewControllers![selectedIndex] as! UINavigationController
            cu_vc.pushViewController(vc, animated: true)            
        }
    }
}

