//
//  UITableView+re.swift
//  Skycar
//
//  Created by 温仲斌 on 2018/6/19.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import UIKit
import MJRefresh

//MARK:MJRefresh
extension UITableView {
    
    func refresh() {
        self.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.refreshHeaderComplete()
        })
        
        self.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: { [weak self] in
            self?.refreshFooterComplete()
        })
    }
    
    @objc func refreshFooterComplete() {
        refeshEnd()
    }
    
    @objc func refreshHeaderComplete() {
        refeshEnd()
    }
    
    @objc func refeshEnd() {
        if self.mj_footer != nil {
            self.mj_footer.endRefreshing()
        }
        if self.mj_header != nil {
            self.mj_header.endRefreshing()
        }
    }
    
    @objc func refreshNoData() {
        self.mj_footer.endRefreshingWithNoMoreData()
    }
    
    @objc func resetNoMoreData() {
        self.mj_footer.resetNoMoreData()
        
    }
    
    func refresh_header(_ title : String?) {
        
        self.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.refreshHeaderComplete()
        })
        (self.mj_header as! MJRefreshNormalHeader).lastUpdatedTimeLabel.isHidden = true
        if title != nil {
            (self.mj_header as! MJRefreshNormalHeader).setTitle(title, for: MJRefreshState.pulling)
            (self.mj_header as! MJRefreshNormalHeader).setTitle(title, for: MJRefreshState.refreshing)
            (self.mj_header as! MJRefreshNormalHeader).setTitle(title, for: MJRefreshState.idle)
        }
        
        
    }
}

//MARK:提示view
//extension UITableView {
//    
//    func is_show_placeholder_view(code:Int) {
//        guard code == 0 else {
//            remove_placeholder_view()
//            return
//        }
//        add_placeholder_view(code: code)
//    }
//    
//    func add_placeholder_view(code : Int) {
//        remove_placeholder_view()
//        let placeholder = UIView.nodata_view.data { (v) in
//            code == 0 ? (v as! DW_NoDataView).setNoNetwork() : nil
//        }
//        guard placeholder != nil else {
//            return
//        }
//        
//        weak var vc = parentViewController()
//        vc?.view.addSubview(placeholder)
//        placeholder.snp.makeConstraints { (make) in
//            make.top.left.right.bottom.equalToSuperview()
//        }
//        
//        (placeholder as! DW_NoDataView).reload_block
//            = {[weak self] in
//                self?.reloData()
//        }
//        
//        (placeholder as! DW_NoDataView).pop_block
//            = {
//                vc?.navigationController?.popViewController(animated: true)
//        }
//        
//        vc?.view.bringSubview(toFront: placeholder)
//    }
//    
//    
//    func remove_placeholder_view() {
//        weak var vc = parentViewController()
//        vc?.view.subviews.map { (v) -> Void in
//            if v.classForCoder == DW_NoDataView.classForCoder() {
//                v.removeFromSuperview()
//            }
//        }
//    }
//
//
//}
//
