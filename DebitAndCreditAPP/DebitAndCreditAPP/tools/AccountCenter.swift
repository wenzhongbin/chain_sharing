//
//  AccountCenter.swift
//  Skycar
//
//  Created by hxpp on 2018/6/12.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import Alamofire
import HandyJSON

let APP_ID_URL = "https://itunes.apple.com/cn/lookup?id=1128276672"


public class UserModel: Base_Model {

    var token : String = ""
    var id : String = ""
    var phone : String = ""
    var avator : String = ""
}

class AccountCenter: NSObject,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    static let accountCenter = AccountCenter()
    
    //获取token
    class func token() -> String? {
        let user = UserModel.deserialize(from: UserDefaults.standard.value(forKey: "user") as? [String:Any])
        
        guard user != nil else {
            return ""
        }
        return user?.token
    }
    
    //获取ID
    class func ID() -> String? {
        let user = UserModel.deserialize(from: UserDefaults.standard.value(forKey: "user") as? [String:Any])
        
        guard user != nil else {
            return ""
        }
        return user?.id
    }
    
    //获取Phone
    class func phone() -> String? {
        let user = UserModel.deserialize(from: UserDefaults.standard.value(forKey: "user") as? [String:Any])
        
        guard user != nil else {
            return ""
        }
        return user?.phone
    }
    
    //获取Phone
    class func ac_img() -> String {
        let user = UserModel.deserialize(from: UserDefaults.standard.value(forKey: "user") as? [String:Any])
        
        guard user != nil else {
            return ""
        }
        return user?.avator ?? ""
    }
    
    class func user(to user:UserModel?) {
        UserDefaults.standard.set(user?.toJSON(), forKey: "user")
        UserDefaults.standard.synchronize()
    }
    
    class func register(view : UIView,parameter : [String : Any],complete : (()->())?) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        Net_base_data.networking(URL_PATH.register, parameters: parameter, hud_block: { () -> (MBProgressHUD) in
            return hud
        }, complete: {(obj) in
            hud.hide(animated: true)
            complete?()
        }) {(errMsg,code) in
            
            let hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud.mode = .text
            hud.label.text = errMsg
            hud.hide(animated: true, afterDelay: 2)
        }
    }
    
    class func login(view : UIView,parameter : [String : Any],complete : (()->())?) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "正在登陆..."
        var para = parameter
        para["type"] = "login"
        
        UserModel.networking(URL_PATH.register, parameters: para, complete: { (obj) in
            
            hud.hide(animated: true)
            AccountCenter.user(to: obj as? UserModel)
            complete?()
        }) { (errMsg, _) in
            
            hud.mode = .text
            hud.label.text = errMsg
            hud.hide(animated: true, afterDelay: 2)
            
        }
        
        
    }
}
