//
//  NSString+date.h
//  BJMechanicalElectrica
//
//  Created by hxpp on 2018/10/24.
//  Copyright © 2018年 Dawen. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (date)
//+ (NSString *)stringWithText:(NSString *)string;
+ (NSString *)getCurrTime:(NSString *)string;
+ (NSString *)featureWeekdayWithDate:(NSString *)featureDate;
@end

NS_ASSUME_NONNULL_END
