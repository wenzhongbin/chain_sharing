//
//  CALayer.swift
//  Skycar
//
//  Created by hxpp on 2018/5/23.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit

extension CALayer {
    open override func setValue(_ value: Any?, forKey key: String) {
        if key == "shadowColor" && ((value as? UIColor) != nil) {
            self.shadowColor = (value as! UIColor).cgColor
            return
        }
        
        if key == "borderColor" && ((value as? UIColor) != nil) {
            self.borderColor = (value as! UIColor).cgColor
            return
        }
        super.setValue(value, forKey: key)
    }
}
