//
//  YMAl.swift
//  BJCentreProduct
//
//  Created by 温仲斌 on 2017/6/19.
//  Copyright © 2017年 Shanghai Binjie Electromechanical Co.,Ltd. All rights reserved.
//

import Foundation
import UIKit

class YMAlertView: UIAlertView,UIAlertViewDelegate {
    
    var alertAction : ((Int)->())?
    
    override func show() {
        self.delegate = self
        super.show()
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        alertAction?(buttonIndex)
    }
}
