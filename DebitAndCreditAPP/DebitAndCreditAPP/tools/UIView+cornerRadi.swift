//
//  UIView+cornerRadi.swift
//  Skycar
//
//  Created by hxpp on 2018/5/25.
//  Copyright © 2018年 环球科技. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addCorner(roundingCorners: UIRectCorner, cornerSize: CGSize) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: roundingCorners, cornerRadii: cornerSize)
        let cornerLayer = CAShapeLayer()
        cornerLayer.frame = bounds
        cornerLayer.path = path.cgPath
        self.clipsToBounds = true
        
        layer.mask = cornerLayer
    }
}
