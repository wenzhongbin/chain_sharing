//
//  BaseViewController.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/7/19.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let back = navigationController?.navigationBar.backIndicatorImage = IMG(imStr: "nav_back_black_icon")
//        let backBtn = UIBarButtonItem.init(image: IMG(imStr: "nav_back_black_icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(pop))
//        navigationController?.navigationItem.leftBarButtonItem = backBtn

    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
