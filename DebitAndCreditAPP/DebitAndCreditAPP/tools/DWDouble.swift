//
//  DWDouble.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/9/6.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import Foundation

extension Double {
    func rate() -> String {
        return String.init(format: "%.2f", self) + "%"
    }
    
    func rate_100() -> String {
        return String.init(format: "%.2f", self * 100) + "%"
    }
    
    func rate_decimals_2() -> String {
        let strs = String.init(format: "%.2f", self * 100).components(separatedBy: ".")
        if strs.last?.replacingOccurrences(of: "0", with: "").count == 0 {
            return strs.first!
        }
        return String.init(format: "%.2f", self * 100)
    }
    
    func rate_decimals_0() -> String {
        let strs = String.init(format: "%.2f", self).components(separatedBy: ".")

        return strs.first!
    }
}

extension Int {
    func price() ->String{
        
        return "\(self)".price()
    }
}
