//
//  Const.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/7/31.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import Foundation
import UIKit
import CommonCrypto

let infoDictionary_cost = Bundle.main.infoDictionary!
let appDisplayName_cost = infoDictionary_cost["CFBundleDisplayName"] //程序名称
let majorVersion_cost = infoDictionary_cost["CFBundleShortVersionString"]//主程序版本号
let minorVersion_cost = infoDictionary_cost["CFBundleVersion"]//版本号（内部标示）
let appVersion_cost = majorVersion_cost as! String

//设备信息
let iosVersion_cost = UIDevice.current.systemVersion //iOS版本
let identifierNumber_cost = UIDevice.current.identifierForVendor //设备udid
let systemName_cost = UIDevice.current.systemName //设备名称
let model_cost = UIDevice.current.model //设备型号
let modelName_cost = UIDevice.current.model //设备具体型号
let localizedModel_cost = UIDevice.current.localizedModel //设备区域化型号如A1533

//水波动画
public func rippleEffectTransiton() -> CATransition {
    let an = CATransition.init()
    an.type = "rippleEffect"
    an.subtype = kCATransitionFromLeft
    an.duration = 0.5
    return an
}

public func IMG(imStr : String) -> UIImage? {
    return UIImage.init(named: imStr)
}

public func RGBA(a : CGFloat,b : CGFloat,c : CGFloat,d :CGFloat) -> UIColor{
    return UIColor.init(red: a/255.0, green: b/255.0, blue: c/255.0, alpha: d)
}

public func sc_width() ->CGFloat {
    return UIScreen.main.bounds.width
}

public func sc_height() ->CGFloat {
    return UIScreen.main.bounds.height
}

public func isX() -> Bool {
    
    if UIScreen.main.bounds.height == 812 {
        
        return true
        
    }
    return false
}

class X_Window: UIWindow {
    override var rootViewController: UIViewController? {
        didSet {
            self.layer.add(rippleEffectTransiton(), forKey: "")
        }
    }
}

//MARK:通用block
typealias reload_cell_block = (()->())?

// MARK: 账户类型
public enum USER_TYPE {
    case erp
    case none
}

// MARK: 提示框
public func show_hud_text(text : String,view : UIView) {
    let hud = MBProgressHUD.showAdded(to: view, animated: true)
    hud.label.text = text
    hud.mode = MBProgressHUDMode.text
//    hud.offset = CGPoint.init(x: 0, y: MBProgressMaxOffset)
    hud.hide(animated: true, afterDelay: 3)
}

public func show_hud_succeed(text:String, hud:MBProgressHUD) {
    hud.mode = .customView
    hud.customView = UIImageView.init(image: IMG(imStr: "Checkmark"))
    hud.label.text = text
    hud.hide(animated: true, afterDelay: 2)
}


/**
 
 *  获取当前Month
 
 */
public func getMonth() ->Int {
    
    let calendar = NSCalendar.current
    
    //这里注意 swift要用[,]这样方式写
    
    let com = calendar.dateComponents([.year,.month,.day], from: Date())
    
    return com.month!
    
}


/**
 
 *  获取当前Year
 
 */
public func getYear() ->Int {
    
    let calendar = NSCalendar.current
    
    //这里注意 swift要用[,]这样方式写
    
    let com = calendar.dateComponents([.year,.month,.day], from: Date())

    return com.year!
    
}

// MARK:时间戳转换

public func stringToTimeStamp(stringTime:String)->String {
    
    let timeInterval:TimeInterval = TimeInterval(stringTime) ?? 0
    let date = Date(timeIntervalSince1970: timeInterval / 1000)
    
    //格式话输出
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy年MM月dd日"
    return dformatter.string(from: date)
    
}

public func dateConvertString(date:Date, dateFormat:String="yyyy-MM-dd") -> String {
    let timeZone = TimeZone.init(identifier: "UTC")
    let formatter = DateFormatter()
    formatter.timeZone = timeZone
    formatter.locale = Locale.init(identifier: "zh_CN")
    formatter.dateFormat = dateFormat
    let date = formatter.string(from: date)
    return date.components(separatedBy: " ").first!
}

//生成随机码
public func randomMD5() -> String {
    let identifier = CFUUIDCreate(nil)
    let identifierString = CFUUIDCreateString(nil, identifier) as String
    let cStr = identifierString.cString(using: .utf8)
    var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
    CC_MD5(cStr, CC_LONG(strlen(cStr)), &digest)
    var output = String()
    for i in digest {
        output = output.appendingFormat("%02X", i)
    }
    return output
    
}

//MARK:百度地图 Key
public let BDMKey = "rETjMyT2SN2aIpRGErTwSyQGOZP5XuHt"


