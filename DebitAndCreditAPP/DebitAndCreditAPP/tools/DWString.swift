//
//  DWString.swift
//  HXPPApp_iOS
//
//  Created by hxpp on 2018/9/6.
//  Copyright © 2018年 HXPP technology co., LTD. All rights reserved.
//

import Foundation
import CommonCrypto
extension String {
    func price() -> String {
        
        let strs = self.components(separatedBy: ".")
        
        var p_str = strs.first!
        let install_count = p_str.replacingOccurrences(of: "-", with: "").count % 3 != 0 ? p_str.replacingOccurrences(of: "-", with: "").count / 3 : p_str.replacingOccurrences(of: "-", with: "").count / 3 - 1
        guard install_count > 0 else {
            return self
        }
        for index in 0..<install_count {
            p_str.insert(",", at: p_str.index(p_str.endIndex, offsetBy: -((index + 1) * 3 + index)))
        }
        
        if strs.count > 1 {
            p_str = p_str + (strs.last)!
        }
        
        return p_str
    }
}

extension String {
    
    var length: Int {
        return self.characters.count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)), upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}
//不包含后几个字符串的方法
extension String {
    func dropLast(_ n: Int = 1) -> String {
        return String(characters.dropLast(n))
    }
    var dropLast: String {
        return dropLast()
    }
    
    func md5String() -> String{
        let cStr = self.cString(using: String.Encoding.utf8);
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 16)
        CC_MD5(cStr!,(CC_LONG)(strlen(cStr!)), buffer)
        let md5String = NSMutableString();
        for i in 0 ..< 16{
            md5String.appendFormat("%02x", buffer[i])
        }
        free(buffer)
        return md5String as String

    }
    
    func phone_str() -> String {
        var str = self[0..<3] + "****" + self[7..<11]
        
        return str
    }
    
    
}

extension String {
    
    public func friendlyTime(dateTime: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "zh_CN") as Locale!
        dateFormatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd HH:mm:ss")
        if let date = dateFormatter.date(from: dateTime) {
            let delta = NSDate().timeIntervalSince(date)
            
            if (delta <= 0) {
                return "刚刚"
            }
            else if (delta < 60) {
                return "\(Int(delta))秒前"
            }
            else if (delta < 3600) {
                return "\(Int(delta / 60))分钟前"
            }
            else {
                let calendar = NSCalendar.current
                let unitFlags = Set<Calendar.Component>([.year,.month,.day,.hour,.minute])
                let comp = calendar.dateComponents(unitFlags, from: Date())
                
                let currentYear = String(comp.year!)
                let currentDay = String(comp.day!)
                
                let comp2 = calendar.dateComponents(unitFlags, from: date)
                let year = String(comp2.year!)
                let month = String(comp2.month!)
                let day = String(comp2.day!)
                var hour = String(comp2.hour!)
                var minute = String(comp2.minute!)
                
                if comp2.hour! < 10 {
                    hour = "0" + hour
                }
                if comp2.minute! < 10 {
                    minute = "0" + minute
                }
                
                if currentYear == year {
                    if currentDay == day {
                        return "今天 \(hour):\(minute)"
                    } else {
                        return "\(month)月\(day)日 \(hour):\(minute)"
                    }
                } else {
                    return "\(year)年\(month)月\(day)日 \(hour):\(minute)"
                }
            }
        }
        return ""
    }
}

