////
////  Contacts+UIView.swift
////  Skycar
////
////  Created by hxpp on 2018/6/26.
////  Copyright © 2018年 环球科技. All rights reserved.
////
//
//import Foundation
//
//import ContactsUI
//
//extension UIView {
//    //创建通讯录
//    func contacts(){
//        //创建通讯录
//        if #available(iOS 9.0, *) {
//            let store = CNContactStore.init()
//        } else {
//            // Fallback on earlier versions
//        };
//        //获取授权状态
//        if #available(iOS 9.0, *) {
//            let AuthStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
//        } else {
//            // Fallback on earlier versions
//        };
//        
//        //没有授权
//        if AuthStatus == CNAuthorizationStatus.notDetermined{
//            
//            //用户授权
//            store.requestAccess(for: CNEntityType.contacts, completionHandler: { (isLgranted, error) in
//                if isLgranted == true{
//                    print("授权成功");
//                    self.getPhoneList();
//                }else{
//                    print("授权失败");
//                }
//                
//            });
//            
//        }else{
//            getPhoneList();
//        }
//        
//    }
//    
//    //获取通讯录
//    func getPhoneList(){
//        //创建通讯录
//        let store = CNContactStore.init();
//        //获取授权状态
//        let AuthStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts);
//        
//        if AuthStatus == CNAuthorizationStatus.authorized{
//            
//            //获取所有的联系人
//            let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey];
//            let request = CNContactFetchRequest.init(keysToFetch: keys as [CNKeyDescriptor]);
//            try?store.enumerateContacts(with: request, usingBlock: { (contact, iStop) in
//                
//                //姓
//                let firstName = contact.familyName;
//                //名
//                let lastName = contact.givenName;
//                print(firstName,lastName);
//                let phoneArr = contact.phoneNumbers;
//                var phonetextArr:Array<String> = Array.init();
//                for labelValue in phoneArr{
//                    let cnlabelV = labelValue as CNLabeledValue;
//                    let value = cnlabelV.value;
//                    
//                    let phoneValue = value.stringValue;
//                    let phoneLabel = cnlabelV.label;
//                    print(phoneLabel,phoneValue);
//                }
//                
//            });
//            
//        }
//        
//    }
//    
//}
