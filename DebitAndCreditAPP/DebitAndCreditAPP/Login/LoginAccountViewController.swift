//
//  LoginAccountViewController.swift
//  AzbApp
//
//  Created by hxpp on 2018/9/14.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class LoginAccountViewController: UIViewController {
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var password_link: UIView!
    @IBOutlet weak var password_tf: UITextField!
    @IBOutlet weak var code_link: UIView!
    @IBOutlet weak var account_tf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        password_tf.reactive.controlEvents(.editingDidBegin).observeValues { [weak self](text) in
            self?.password_link.backgroundColor = "ff6764".toUIColor()
        }
        
        password_tf.reactive.controlEvents(.editingDidEnd).observeValues { [weak self](text) in
            self?.password_link.backgroundColor = "dddddd".toUIColor()
        }
        
        account_tf.reactive.controlEvents(.editingDidBegin).observeValues { [weak self](text) in
            self?.code_link.backgroundColor = "ff6764".toUIColor()
        }
        
        account_tf.reactive.controlEvents(.editingDidEnd).observeValues { [weak self](text) in
            self?.code_link.backgroundColor = "dddddd".toUIColor()
        }
        
        btn.reactive.isEnabled <~ Signal.combineLatest(password_tf.reactive.continuousTextValues, account_tf.reactive.continuousTextValues).map({[weak self] (a) -> Bool in
            let (b,c) = a
            let flag =  (b?.count)! > 0 && ((c?.count)! > 0)
            if flag {
                self?.btn.backgroundColor = "ff6764".toUIColor()
            }else {
                self?.btn.backgroundColor = "dddddd".toUIColor()
            }
            return flag
        })
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func scheduled() {
        //        weak var self_ = self
        //
        //        scheduledDispatchTimer(total: 60, handler: { (total) in
        //
        //            self_?.time_btn.setTitle("\(total)s", for: UIControlState.disabled)
        //            self_?.time_btn.isEnabled = false
        //        }, cancelHandler: {
        //            self_?.time_btn.setTitle("重发", for: UIControlState.normal)
        //            self_?.time_btn.isEnabled = true
        //            self_?.time_btn.layer.borderWidth = 1
        //        }, sender: self.view)
    }
    
    @IBAction func ac_xy(_ sender: Any) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        account_tf.becomeFirstResponder()
    }
    
    @IBAction func ac_login(_ sender: Any) {
        self.view.endEditing(false)
        
        AccountCenter.login(view: self.view, parameter: ["userName" : account_tf.text!, "password" : password_tf.text!.md5String()]) {
            self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ac_pop(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
