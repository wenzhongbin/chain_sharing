//
//  RegisterVerifyViewController.swift
//  AzbApp
//
//  Created by hxpp on 2018/9/14.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit
import ReactiveSwift

class RegisterVerifyViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var time_bt: UIButton!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var code_link: UIView!
    @IBOutlet weak var code_tf: UITextField!
    @IBOutlet weak var title_label: UILabel!
    
//    @IBOutlet weak var dz_tf: UITextField!
    
    var phone : String!
    var sex = 1
    
    @IBOutlet weak var zw_tf: UITextField!
    @IBOutlet weak var gs_tf: UITextField!
    @IBOutlet weak var name: UITextField!
    
    var parameter : [String : Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        code_tf.reactive.controlEvents(.editingDidBegin).observeValues { [weak self](text) in
            self?.code_link.backgroundColor = "ff6764".toUIColor()
        }
        
        code_tf.reactive.controlEvents(.editingDidEnd).observeValues { [weak self](text) in
            self?.code_link.backgroundColor = "dddddd".toUIColor()
        }
        
        btn.reactive.isEnabled <~ Signal.combineLatest(code_tf.reactive.continuousTextValues, name.reactive.continuousTextValues, gs_tf.reactive.continuousTextValues).map({[weak self] (a) -> Bool in
            let (b,c,d) =  a
            if b!.count > 0 && c!.count > 0 && d!.count > 0  {
                self?.btn.backgroundColor = "ff6764".toUIColor()
            }else {
                self?.btn.backgroundColor = "dddddd".toUIColor()
            }
            
            return b!.count > 0 && c!.count > 0 && d!.count > 0
        })
        
        
        name.reactive.controlEvents(.editingChanged).observeValues { [weak self](text) in
        }
        
        gs_tf.reactive.controlEvents(.editingChanged).observeValues { [weak self](text) in
        }
        
        btn.reactive.controlEvents(UIControlEvents.touchUpInside).observeValues { (b) in
            
            AccountCenter.register(view: self.view, parameter: ["type": "register", "userName" :self.code_tf.text!, "password" : self.name.text!.md5String(), "phone" : self.gs_tf.text!,"sex" : self.sex], complete: {
                show_hud_succeed(text: "注册成功!", hud: MBProgressHUD.showAdded(to: self.view, animated: true))
                DispatchAfter(after: 2, handler: {
                    AccountCenter.login(view: self.view, parameter: ["userName" : self.code_tf.text!, "password" : self.name.text!.md5String()], complete: {
                        self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()

                    })
                })
            })
        }
        
        zw_tf.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == zw_tf {
            
            let al = UIAlertController.init(title: "性别", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            al.addAction(UIAlertAction.init(title: "男", style: UIAlertActionStyle.default, handler: {[weak self] (ac) in
                textField.text = "男"
                self?.sex = 1
            }))
            
            al.addAction(UIAlertAction.init(title: "女", style: UIAlertActionStyle.default, handler: { [weak self](ac) in
                textField.text = "女"
                self?.sex = 2
            }))
            
            al.addAction(UIAlertAction.init(title: "取消", style: UIAlertActionStyle.cancel, handler: { (ac) in
                
            }))
            
            self.present(al, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
