//
//  HomeViewController.swift
//  make_love
//
//  Created by 温仲斌 on 2018/7/23.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let l = Bundle.main.loadNibNamed("HomeHeaderView", owner: nil, options: nil)?.first as! HomeHeaderView
        l.bounds = CGRect.init(x: 0, y: 0, width: 200, height: 44)
        
        l.slide_scr = {[weak self](sc) in
            (self?.view as! HomeView).scrollView.contentOffset = CGPoint.init(x: sc, y: 0)
        }
        
        (view as! HomeView).slide_scr = l.slide_view
        
        
        self.navigationItem.titleView = l
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
