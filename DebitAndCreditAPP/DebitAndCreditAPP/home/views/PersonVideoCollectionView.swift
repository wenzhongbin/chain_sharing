//
//  PersonVideoCollectionView.swift
//  make_love
//
//  Created by hxpp on 2018/7/26.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class PersonVideoCollectionView: UICollectionView,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
    }
    
}

extension PersonVideoCollectionView {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 150)
    }
    
}
