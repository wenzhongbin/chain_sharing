//
//  HomeView.swift
//  make_love
//
//  Created by hxpp on 2018/7/24.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class HomeView: UIView,UIScrollViewDelegate {
   
    let view1 : HomeDataViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDataViewController_1") as! HomeDataViewController
    
    let view2 : HomeDataViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDataViewController_2") as! HomeDataViewController
    
    let view3 : HomeDataViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDataViewController_3") as! HomeDataViewController

    @IBOutlet weak var scrollView: UIScrollView!

    
    var slide_scr : ((_ sc : UIScrollView) ->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.contentSize = CGSize.init(width: sc_width() * 3, height: scrollView.height)
        scrollView.delegate = self
        view1.view.height = scrollView.height
        view2.view.height = scrollView.height
        view3.view.height = scrollView.height
        scrollView.setContentOffset(CGPoint.init(x: sc_width(), y: 0), animated: true)
        
        view1.tableView.type = 1
        view2.tableView.type = 2
        view3.tableView.type = 3
        
        scrollView.addSubview(view1.view)
        scrollView.addSubview(view2.view)
        scrollView.addSubview(view3.view)
        
        view1.view.centerX = sc_width() / 2
        view2.view.centerX = sc_width() / 2 + sc_width()
        view3.view.centerX = sc_width() / 2 + 2 * sc_width()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        slide_scr?(scrollView)
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
