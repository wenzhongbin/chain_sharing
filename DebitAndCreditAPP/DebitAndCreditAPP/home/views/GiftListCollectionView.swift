//
//  GiftListCollectionView.swift
//  make_love
//
//  Created by hxpp on 2018/7/26.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class GiftListCollectionView: UICollectionView,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    
    var reload_layout : ((_ height : CGFloat)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        
        addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let size = (change![NSKeyValueChangeKey.newKey] as! NSValue).cgSizeValue
        reload_layout?(size.height)
    }
    
}

extension GiftListCollectionView {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_gift", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 60, height: 60)
    }

}
