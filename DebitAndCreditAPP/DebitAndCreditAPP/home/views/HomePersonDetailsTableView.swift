//
//  HomePersonDetailsTableView.swift
//  make_love
//
//  Created by 温仲斌 on 2018/7/25.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class HomePersonDetailsTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
    
    var offset : CGFloat = 0.0
    
    var silde_offset : ((_ f : CGFloat)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        separatorStyle = .none
        rowHeight = UITableViewAutomaticDimension
        contentOffset = CGPoint.zero
        offset = contentOffset.y
        
        refresh_header("姐不是奥利奥你想泡就能泡")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let y = contentOffset.y
        
        let off_y = y - offset
        
        silde_offset?(off_y / 80)
    }
    
}

extension HomePersonDetailsTableView {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = dequeueReusableCell(withIdentifier: "cell_1", for: indexPath) as! GiftListCell
            cell.reloadBlock = {[weak self]() in
                self?.reloadData()
            }
            cell.height_layout.constant = cell.h
            return cell
        }
        
        let cell = dequeueReusableCell(withIdentifier: "cell_video", for: indexPath)
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
    }
}

class HomePersonDetailsHeaderCell: UITableViewCell {
    
    @IBOutlet weak var rovingChartView: RovingChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rovingChartView.sc?.page_type = .right
    }
}

class GiftListCell: UITableViewCell {
    @IBOutlet weak var collectionView: GiftListCollectionView!
    
    @IBOutlet weak var height_layout: NSLayoutConstraint!
    
    var h : CGFloat = 0
    var isReload = true
    
    var reloadBlock : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        height_layout.constant = h
        
        collectionView.reload_layout = {[weak self](h) in
            self?.h = h
            (self?.isReload)! ? self?.reloadBlock?() : nil
            self?.isReload = false
        }
    }
}

class VideoListCell: UITableViewCell {
    
    @IBOutlet weak var height_layout: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        height_layout.constant = 160
    }
}
