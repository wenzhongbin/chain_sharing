//
//  HomeHeaderView.swift
//  make_love
//
//  Created by hxpp on 2018/7/24.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class HomeHeaderView: UIView {

    var slide_scr : ((_ sd : CGFloat) ->())?
    var slide_view : ((_ sc : UIScrollView) ->())?
    @IBOutlet weak var slideView: UIView!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var slide_x: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        slide_view = {[weak self] (scrollView) in
            let x1 = fabs((self?.btn3.centerX)! - (self?.btn1.centerX)!)
            
            let x2 = 2 * scrollView.width
            
            let x4 = x1 * scrollView.contentOffset.x / x2
            
            self?.slideView.centerX = (self?.btn1.centerX)! + x4            
        }
    }    
    

    @IBAction func ac_slide(_ sender: Any) {
        UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            self.slideView.centerX = (sender as! UIButton).centerX
            self.slide_scr?(CGFloat(((sender as! UIButton).tag % 1000)) * sc_width())            
        }) { (b) in
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return UILayoutFittingExpandedSize
        }
    }
    
}
