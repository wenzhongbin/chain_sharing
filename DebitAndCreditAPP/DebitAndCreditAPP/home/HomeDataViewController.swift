//
//  HomeDataViewController.swift
//  make_love
//
//  Created by hxpp on 2018/7/24.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

class HomeDataViewController: UIViewController {

    @IBOutlet weak var tableView: MainDetailTableView!
    var type = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.type = type
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
