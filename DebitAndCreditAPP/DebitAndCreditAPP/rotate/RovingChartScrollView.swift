//
//  RovingChartScrollView.swift
//  make_love
//
//  Created by hxpp on 2018/7/25.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit
import SnapKit

class RovingChartScrollView: UIScrollView,UIScrollViewDelegate,CAAnimationDelegate {
    let image_1 = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: sc_width(), height: 0))
    let image_2 = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: sc_width(), height: 0))
    let image_3 = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: sc_width(), height: 0))
    
    let pageControl = UIPageControl.init(frame:  CGRect.init(x: 0, y: 0, width: sc_width(), height: 20))
    
    var index = 0
    
    var page_type : RovingChartPage? {
        
        didSet {
            switch page_type {
            case .left?:
                pageControl.centerY = height - 10
                pageControl.left = 0
                pageControl.width = sc_width() / 4
            case .center?:
                pageControl.centerY = height - 10
            case .right?:
                pageControl.centerY = height - 10
                pageControl.left = 3 * sc_width() / 4
                pageControl.width = sc_width() / 4
            default:
                pageControl.centerY = height - 10
                break
            }
        }
    }
    
    var images : [String] = ["h1", "h2", "h3", "h4"]
    
    var isAuto : Bool = true
    
    weak var timer : DispatchSourceTimer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addImageViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addImageViews()
    }
    
    override func didMoveToSuperview() {
        superview?.addSubview(pageControl)
    }
    
    func addImageViews() {
        image_1.height = height
        image_2.height = height
        image_3.height = height
        image_2.centerX = sc_width() + sc_width() / 2
        image_3.centerX = 2 * sc_width() + sc_width() / 2
        
        pageControl.currentPageIndicatorTintColor = UIColor.red
        
        addSubview(image_1)
        addSubview(image_2)
        addSubview(image_3)
        
        contentSize = CGSize.init(width: 3 * sc_width(), height: height)
        showsVerticalScrollIndicator = false
        showsVerticalScrollIndicator = false
        contentOffset = CGPoint.init(x: sc_width(), y: 0)
        conf()
    }
    
    func conf() {
        delegate = self
        isPagingEnabled = true
        
        pageControl.numberOfPages = images.count
        pageControl.currentPage = index
        
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        
        setImg()
        
        DispatchTimer(timeInterval: 4) { [weak self](timer) in
            self?.autoScroll()
        }
    }
    
    func autoScroll() {
        image_2.layer.removeAllAnimations()
        guard isAuto else {
            return
        }
        let an = CATransition.init()
        an.type = "rippleEffect"
        an.subtype = kCATransitionFromLeft
        an.duration = 0.5
        an.delegate = self
        image_2.layer.add(an, forKey: " ")
    }
    
    func animationDidStart(_ anim: CAAnimation) {
        guard isAuto else {
            return
        }
        index = (index + 1) % images.count
        pageControl.currentPage = index
        image_2.image = IMG(imStr: images[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setImg()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isAuto = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.x > sc_width() {
            index = (index + 1 + images.count) % images.count
        }else if scrollView.contentOffset.x < sc_width() {
            index = (index - 1 + images.count) % images.count
        }
        setImg()
        
        DispatchAfter(after: 2) {
            self.isAuto = true
        }
        
        contentOffset = CGPoint.init(x: sc_width(), y: 0)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewDidEndDecelerating(scrollView)
    }
    
    func setImg() {        
        image_1.image = IMG(imStr: images[(index - 1 + images.count) % images.count])
        image_2.image = IMG(imStr: images[index])
        image_3.image = IMG(imStr: images[(index + 1 + images.count) % images.count])
        pageControl.currentPage = index
    }
}
