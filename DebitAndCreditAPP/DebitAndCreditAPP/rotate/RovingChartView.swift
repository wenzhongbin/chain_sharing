//
//  RovingChartView.swift
//  make_love
//
//  Created by hxpp on 2018/7/25.
//  Copyright © 2018年 aozibei technology co., LTD. All rights reserved.
//

import UIKit

enum RovingChartPage : Int {
    case left = 0
    case center
    case right
}

class RovingChartView: UIView {
    
    var page_type : RovingChartPage? {
        didSet(newValue) {
            sc?.page_type = newValue
        }
    }
    
    var sc : RovingChartScrollView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        sc = RovingChartScrollView.init(frame: frame)
        sc?.page_type = .center
        addSubview(sc!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sc = RovingChartScrollView.init(frame: frame)
        sc?.page_type = .center
        addSubview(sc!)
    }
    
}
