//
//  UserTableView.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/25.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit

class UserTableView: UITableView {

    let titles = ["用户协议", "隐私条款", "关于我们"]
    let urls = ["user/item_content","user/personal_privacy", "user/app_information"]

    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        rowHeight = UITableViewAutomaticDimension
    }
}

extension UserTableView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = titles[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.url = DNS + urls[indexPath.row]
        vc.title_name = titles[indexPath.row]
        UIView.push(vc: vc)
    }
}
