//
//  UserCententViewController.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/25.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit

class UserCententViewController: UIViewController,TZImagePickerControllerDelegate {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "个人中心"
        // Do any additional setup after loading the view.
        name.text = AccountCenter.phone()?.phone_str()
        if AccountCenter.ac_img().count > 0 {
            img.image(AccountCenter.ac_img())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        name.text = AccountCenter.phone()?.phone_str()
        if AccountCenter.ac_img().count > 0 {
            img.image(AccountCenter.ac_img())
        }
    }
    
    @IBAction func ac_logout(_ sender: Any) {
        
        UserDefaults.standard.set("", forKey: "token")
        UserDefaults.standard.synchronize()
        self.view.window?.rootViewController = UIStoryboard.init(name: "Login", bundle: nil).instantiateInitialViewController()
    }
    
    @IBAction func ac_change_img(_ sender: Any) {
        let vc = TZImagePickerController.init(maxImagesCount:1, delegate: self)
        
        self.present(vc!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .determinateHorizontalBar
        hud.label.text = "上传中..."
        
        let data = try? JSONSerialization.data(withJSONObject: ["type" : "img", "id" : AccountCenter.ID()], options: JSONSerialization.WritingOptions.init(rawValue: 0))
        var pr : [(String,Any)] = [("data", data)]
        pr.append(("photo",UIImagePNGRepresentation(photos.first!)))
        
        Net_base_data.networking_Form_body(URL_PATH.avator_deal, parameters: pr, complete: { (obj) in
            
            let user = UserModel.deserialize(from: UserDefaults.standard.value(forKey: "user") as? [String:Any])
            user?.avator = (obj as! Net_base_data).imgUrl
            
            AccountCenter.user(to: user)
            
            self.img.image(user!.avator)

            hud.hide(animated: true)
            show_hud_succeed(text: "上传成功!", hud: MBProgressHUD.showAdded(to: self.view, animated: true))
            
        }, error: { (err, _) in
            hud.hide(animated: true)
            show_hud_text(text: "上传失败!", view: self.view)
        }) { (p) in
            DispatchQueue.main.async {
                hud.label.text = p.rate_100()
                hud.progress = Float(p)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
