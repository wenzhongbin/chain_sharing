//
//  WebViewController.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/25.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    var url : String = ""
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var title_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = title_name
               
        webView.delegate = self
        indicatorView.startAnimating()
        
        if URL.init(string: url) != nil {
            let re = URLRequest.init(url: URL.init(string: url)!)
            webView.loadRequest(re)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        indicatorView.stopAnimating()
        indicatorView.isHidden = true
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
