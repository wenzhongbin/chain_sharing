//
//  ZJKTableView.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/31.
//  Copyright © 2018 AZB technology co., LTD. All rights reserved.
//

import UIKit

class ZJKTableView: UITableView {
    
    var models : [DyDataModels] = [] {
        didSet {
            reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        rowHeight = UITableViewAutomaticDimension
        refresh_header("我们竭诚为您服务~")
        self.mj_header.beginRefreshing()
    }
    
    override func reloData() {
        
        let pr = ["type" : "lst", "userID" : AccountCenter.ID() ?? "", "cursor" : nil]
        
        DyDataModels.networking(URL_PATH.article_data, parameters: pr,  complete: {[weak self] (obj) in
            self?.models = obj as! [DyDataModels]
            self?.refeshEnd()
        }) { (errMsg,code) in
            
            self.refeshEnd()
        }
        
    }
    
    override func refreshHeaderComplete() {
        reloData()
    }
    
}

extension ZJKTableView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ZJKTableViewCell
        cell.data(models[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
    }
}


class ZJKTableViewCell: UITableViewCell,YBImageBrowserDataSource {
    func imageViewOfTouch(for imageBrowser: YBImageBrowser) -> UIImageView? {
        return image_views[Int(imageBrowser.currentIndex)]
    }
    
    func number(in imageBrowser: YBImageBrowser) -> Int {
        return image_views.count
    }
    
    func yBImageBrowser(_ imageBrowser: YBImageBrowser, modelForCellAt index: Int) -> YBImageBrowserModel {
    
        let m = YBImageBrowserModel.init()
        
        m.url = URL.init(string: model.imgs()[index])
        
        
        return m
    }
    
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var imgs_view: UIView!
    @IBOutlet weak var content_title: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image_view_height: NSLayoutConstraint!
    var tmp = 0
    var model : DyDataModels!
    var image_views : [UIImageView] = []
    
    func data(_ model : Any) {
        var model = model as! DyDataModels
        self.model = model
        name.text = model.userName
        img.image(model.avator)
        content_title.text = model.content
        
        for v in imgs_view.subviews {
            
            v.removeFromSuperview()
        }
        
        image_views = []

        for i in 0..<model.imgs().count {
            let img_ = model.imgs()[i]
            
            let img_view = UIImageView.init()
            image_views.append(img_view)
            img_view.image(img_)
//            img_view.backgroundColor = UIColor.gray
            img_view.tag = 1000 + i
            let tap_ = UITapGestureRecognizer.init(target: self, action: #selector(tap(tap:)))
            
            img_view.addGestureRecognizer(tap_)
            img_view.isUserInteractionEnabled = true
            
            let w = (imgs_view.width - 30) / 3
            let h = w
            img_view.frame.size = CGSize.init(width: w, height: h)
            
            var x : CGFloat = 0.0
            var y : CGFloat = 0.0
            switch i {
            case 0,1,2:
                x = (imgs_view.width - 30) / 3
                x = CGFloat(i) * x + CGFloat(i * 5 + 10) + CGFloat(x / 2)
                y = 55
                tmp = Int(2 * w / 2 + 10)
            case 3,4,5:
                x = (imgs_view.width - 30) / 3
                x = CGFloat(i % 3) * x + CGFloat((i % 3) * 5 + 10) + CGFloat(x / 2)
                y = 3 * w / 2 + 15
                tmp = Int(4 * w / 2 + 15)
            case 6,7,8:
                x = (imgs_view.width - 30) / 3
                x = CGFloat(i % 3) * x + CGFloat((i % 3) * 5 + 10) + CGFloat(x / 2)
                y = 5 * w / 2 + 20
                tmp = Int(6 * w / 2 + 20)
            default:
                break
            }
            
            img_view.center = CGPoint.init(x: x, y:  y)
            imgs_view.addSubview(img_view)
            
            
        }
        
        image_view_height.constant = CGFloat(tmp)
        
        time.text = model.create_time.friendlyTime(dateTime: model.create_time)
        layoutIfNeeded()
    }
    
    @objc func tap(tap : UITapGestureRecognizer) {
        let b = YBImageBrowser.init()
        b.dataSource = self
        b.currentIndex = UInt(tap.view!.tag - 1000)
        b.show()
    }
    
    
}
