//
//  AddDTTableView.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/31.
//  Copyright © 2018 AZB technology co., LTD. All rights reserved.
//

import UIKit

class AddDTTableView: UITableView {
    
    var model : AddDTModel = AddDTModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        rowHeight = UITableViewAutomaticDimension
        separatorStyle = .none
    }
    
}

extension AddDTTableView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            let cell = dequeueReusableCell(withIdentifier: "cell_1", for: indexPath) as! AddImageViewTableCell
            
            cell.data(self.model)
            
            cell.reload_h = { (images) in
                //                cell.images = images
                //                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                
                self.reloadData()
                
            }
            return cell
        }
        
        let cell = dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ADDTFViewTableCell
        cell.data(self.model)

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
    }
}

class ADDTFViewTableCell: UITableViewCell,UITextViewDelegate {
    
    @IBOutlet weak var content_tf: UITextView!
    
    var relod_data : ((_ text:String)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        content_tf.placeholder = "这一刻的想法..."
        content_tf.becomeFirstResponder()
        relod_data?(content_tf.text)
        
        content_tf.delegate = self
    }
    
    var model : AddDTModel!
    func data(_ model : AddDTModel) {
        self.model = model
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        model.content_str = textView.text! + text
        return true
    }
}

class AddImageViewTableCell: UITableViewCell,TZImagePickerControllerDelegate {
    
    
    @IBOutlet weak var h: NSLayoutConstraint!
    @IBOutlet weak var btn: UIButton!
    
    @IBOutlet weak var content_view: UIView!
    

    
    var reload_h : ((_ images : [UIImage])->())?
    var tmp = 100
    var images : [UIImage] = [IMG(imStr: "plus")!]
    
    var model : AddDTModel!
    func data(_ model : AddDTModel) {
        self.model = model
        
        for v in content_view.subviews {
            if v == btn {
                continue
            }
            v.removeFromSuperview()
        }
        
        for i in 0..<images.count {
            let img = images[i]
            
            let img_view = UIImageView.init(image: img)
            let w = (content_view.width - 30) / 3
            let h = w
            img_view.frame.size = CGSize.init(width: w, height: h)
            
            var x : CGFloat = 0.0
            var y : CGFloat = 0.0
            switch i {
            case 0,1,2:
                x = (content_view.width - 30) / 3
                x = CGFloat(i) * x + CGFloat(i * 5 + 10) + CGFloat(x / 2)
                y = 55
                tmp = Int(2 * w / 2 + 10)
            case 3,4,5:
                x = (content_view.width - 30) / 3
                x = CGFloat(i % 3) * x + CGFloat((i % 3) * 5 + 10) + CGFloat(x / 2)
                y = 3 * w / 2 + 10
                tmp = Int(4 * w / 2 + 15)
            case 6,7,8:
                x = (content_view.width - 30) / 3
                x = CGFloat(i % 3) * x + CGFloat((i % 3) * 5 + 10) + CGFloat(x / 2)
                y = 5 * w / 2 + 15
                tmp = Int(6 * w / 2 + 20)
            default:
                break
            }
            
            if images.count - 1 == i {
                btn.transform = CGAffineTransform.init(translationX: x - w / 2, y: y - w / 2)
            }else {
                img_view.center = CGPoint.init(x: x, y:  y)
                content_view.addSubview(img_view)
            }
            
            
        }
        
        
        
        btn.isHidden = images.count >= 9
        
        
        
        h.constant = CGFloat(tmp)
        
        
    }
    
    @IBAction func ac_add(_ sender: Any) {
        
        
        
        let vc = TZImagePickerController.init(maxImagesCount:10 - images.count, delegate: self)
        
        
        parentViewController()?.present(vc!, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        self.images.removeLast()
        self.images.append(contentsOf: photos)
        self.model.images = images
        self.images.append(IMG(imStr: "plus")!)
        reload_h?(self.images)
    }
    
    
}
