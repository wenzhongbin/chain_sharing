//
//  DataModels.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/25.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import Foundation

class DataModels: Base_Model {
    var id : String = ""
    var name : String = ""
    var imgUrl : String = ""
    var links : String = ""
    var limitStart : Int = 0
    var limitEnd : Int = 0
    var interest : Double = 0.00
    var isHot : Int = 0
    var isNew : Int = 0
    var applyNumber : Int = 0
}


class DyDataModels: Base_Model {
    var id : String = ""
    var userID : String = ""
    var content : String = ""
    var imgUrl : String = ""
    var goodNumber : String = ""
    var create_time : String = ""
    var userName : String = ""
    var avator : String = ""
    
    func imgs() -> [String] {
        var arr = imgUrl.components(separatedBy: ",")
        arr.removeLast()
        return arr
    }
}
