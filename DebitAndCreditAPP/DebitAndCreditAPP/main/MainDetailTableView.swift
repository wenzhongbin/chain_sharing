//
//  MainDetailTableView.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/22.
//  Copyright © 2018年 AZB technology co., LTD. All rights reserved.
//

import UIKit

class MainDetailTableView: UITableView {
    var type = 1
    var models : [DataModels] = []{
        didSet {
            reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        rowHeight = UITableViewAutomaticDimension
        refresh_header("我们竭诚为您服务~")
        self.mj_header.beginRefreshing()
    }
    
    override func reloData() {
        var keyword = ""
        switch type {
        case 1:
            keyword = "isHot"
        case 2:
            keyword = "name"
        case 3:
            keyword = "isNew"
        case 4:
            keyword = "data"
        default:
            break
        }
        
        DataModels.networking(URL_PATH.content_data, parameters: ["keywords": keyword, "id" : AccountCenter.ID() ?? ""],  complete: {[weak self] (obj) in
            self?.models = obj as! [DataModels]
            self?.refeshEnd()
        }) { (errMsg,code) in

            self.refeshEnd()
        }
       
    }
    
    override func refreshHeaderComplete() {
        reloData()
    }
    
}

extension MainDetailTableView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DataTableViewCell
        cell.data(models[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        deselectRow(at: indexPath, animated: true)
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.url = models[indexPath.row].links
        vc.title_name = models[indexPath.row].name
        UIView.push(vc: vc)
//        UIView.push(vc: UIViewController())
    }
    
    
}

class DataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sqrs_label: UILabel!
    @IBOutlet weak var lx_label: UILabel!
    @IBOutlet weak var ed_label: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    func data(_ model : DataModels) {
        img.image(model.imgUrl)
        name.text = model.name
        ed_label.text = "\(model.limitStart)-\(model.limitEnd)元"
        lx_label.text = model.interest.rate()
        sqrs_label.text = "已有\(model.applyNumber)人申请"
    }
}
