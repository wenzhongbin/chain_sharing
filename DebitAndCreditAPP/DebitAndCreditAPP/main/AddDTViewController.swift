//
//  AddDTViewController.swift
//  DebitAndCreditAPP
//
//  Created by hxpp on 2018/10/31.
//  Copyright © 2018 AZB technology co., LTD. All rights reserved.
//

import UIKit

public class AddDTModel {
    var content_str : String = ""
    var images : [UIImage] = []
}

class AddDTViewController: UIViewController {

    @IBOutlet weak var tableView: AddDTTableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let item = UIBarButtonItem.init(title: "发布", style: UIBarButtonItemStyle.done, target: self, action: #selector(issue_dy))
        self.navigationItem.rightBarButtonItem = item
    }
    
    @objc func issue_dy() {
        
        self.view.endEditing(false)
        DDump(obj: tableView.model.content_str)
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .determinateHorizontalBar
        hud.label.text = "发布中..."
        var count = tableView.model.images.count == 0 ? -1 : tableView.model.images.count
        
        let str = "{" + "\"id\"" + ":" + "\"\(AccountCenter.ID()!)\"" + "," + "\"content\"" + ":" + "\"\(tableView.model.content_str)\"" + "," + "\"type\"" + ":" + "\"add\"" + "," + "\"img_count\"" + ":" + "\(count)" + "}"
        let data = str.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        
//        let data = try? JSONSerialization.data(withJSONObject: ["type" : "add", "id" : AccountCenter.ID(), "content" : tableView.model.content_str, "img_count" : count], options: JSONSerialization.WritingOptions.init(rawValue: 0))
    
        
        var pr : [(String,Any)] = [("data", data)]
        for i in 0..<tableView.model.images.count {
            let img = tableView.model.images[i]
            pr.append(("photo\(i)",UIImagePNGRepresentation(img)))
        }
        
        Net_base_data.networking_Form_body(URL_PATH.article_deal, parameters: pr, complete: { (obj) in
            hud.hide(animated: true)
            show_hud_succeed(text: "发布成功!", hud: MBProgressHUD.showAdded(to: self.view, animated: true))
            DispatchAfter(after: 2, handler: {
                self.navigationController?.popViewController(animated: true)
            })
        }, error: { (errMsg, _) in
            hud.hide(animated: true)
            show_hud_text(text: "发布失败!", view: self.view)
        }) { (p) in
            DispatchQueue.main.async {
                hud.label.text = p.rate_100()
                hud.progress = Float(p)

            }

        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
